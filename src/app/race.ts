import { Pony } from "./pony";

export class Race {
    id: number;
    date: Date;
    location: string;
    ponies: Array<Pony>;

    constructor(id?: number, date?: Date, location?: string, ponies?: Array<Pony>){
        this.id = id == undefined ? 0 : id;
        this.date = date == undefined ? new Date() : date;
        this.location = location == undefined ? "n/a" : location;
        this.ponies = ponies == undefined ? [] : ponies;
    }
}
