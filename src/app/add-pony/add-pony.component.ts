import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Pony } from '../pony';
import { PonyService } from '../pony.service';

@Component({
  selector: 'app-add-pony',
  templateUrl: './add-pony.component.html',
  styleUrls: ['./add-pony.component.css']
})
export class AddPonyComponent implements OnInit {

  pony: Pony;
  updatePony: boolean = false;
  constructor(private router: Router, private service: PonyService, private route: ActivatedRoute) { 
    this.pony = new Pony(0,"","",0);
  }

  ngOnInit(): void {
    this.route.params.subscribe(p => {
      if(p.id != undefined) {
        this.updatePony = true;
        this.service.getPony(p.id).subscribe(pony => {
          this.pony = pony;
        });
      }else{
        this.pony = new Pony(0, "", "", 0);
      }
    });
  }

  onSubmit(): void{
    //PONIES.push(this.pony);
    this.updatePony ? this.service.updatePony(this.pony) : this.service.addPony(this.pony);
    this.service.addPony(this.pony);
    this.router.navigate(['/']);
  }
}
