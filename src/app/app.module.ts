import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes} from '@angular/router';
import { FormArray, FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { PoniesComponent } from './ponies/ponies.component';
import { RacesComponent } from './races/races.component';
import { PonyDetailsComponent } from './pony-details/pony-details.component';
import { RaceDetailsComponent } from './race-details/race-details.component';
import { PoneyNamePipe } from './pipes/poney-name.pipe';
import { MenuComponent } from './menu/menu.component';
import { AddPonyComponent } from './add-pony/add-pony.component';
import { AddRaceComponent } from './add-race/add-race.component';
import { AddPonyReactiveComponent } from './add-pony-reactive/add-pony-reactive.component';
import { AddRaceReactiveComponent } from './add-race-reactive/add-race-reactive.component';
import { PonyService } from './pony.service';
import { HttpClientModule } from '@angular/common/http';
import {PickListModule} from 'primeng/picklist';
import { LocationNamePipe } from './pipes/location-name.pipe';
import {ButtonModule} from 'primeng/button';
import {MenubarModule} from 'primeng/menubar';
import {MenuItem} from 'primeng/api';
import { RaceService } from './race.service';
import { GuardGuard } from './guard.guard';
import { TableModule } from 'primeng/table';
import { NotFoundComponent } from './not-found/not-found.component';

const appRoutes : Routes = [
  {path: '', component: PoniesComponent},
  {path: 'races', component: RacesComponent},
  {path: 'add-pony', component: AddPonyComponent, canActivate: [GuardGuard]},
  {path: 'add-race', component: AddRaceComponent, canActivate: [GuardGuard]},
  {path: 'add-pony2', component: AddPonyReactiveComponent},
  {path: 'add-race2', component: AddRaceReactiveComponent},
  { path: 'update-pony/:id', component: AddPonyComponent, canActivate: [GuardGuard] },
  { path: 'update-race/:id', component: AddRaceComponent, canActivate: [GuardGuard] },
  {path: '**', component: NotFoundComponent}
 
]

@NgModule({
  declarations: [
    AppComponent,
    PoniesComponent,
    RacesComponent,
    PonyDetailsComponent,
    RaceDetailsComponent,
    PoneyNamePipe,
    MenuComponent,
    AddPonyComponent,
    AddRaceComponent,
    AddPonyReactiveComponent,
    AddRaceReactiveComponent,
    LocationNamePipe,
    NotFoundComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    PickListModule,
    ButtonModule,
    MenubarModule,
    TableModule,
  ],
  providers: [
    PonyService,
    RaceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
