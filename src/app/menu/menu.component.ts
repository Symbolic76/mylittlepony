import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  username = ""
  loggedIn : boolean = false;
  items: MenuItem[];
  constructor() {
    this.items = []
   }

  ngOnInit(): void {
    this.items = [
      {
        label: 'Poneys',
        icon: 'pi pi-fw pi-database',
        items: [
          {
            label: 'Ajouter',
            routerLink: 'add-pony',
            icon: 'pi pi-fw pi-plus',
          },
          {
            label: 'Liste',
            routerLink: '/',
            icon: 'pi pi-fw pi-list'
          }
        ]
      },
      {
        label: 'Courses',
        icon: 'pi pi-fw pi-bolt',
        items: [
          {
            label: 'Ajouter',
            routerLink: 'add-race',
            icon: 'pi pi-fw pi-plus',
          },
          {
            label: 'Liste',
            routerLink: 'races',
            icon: 'pi pi-fw pi-list'
          }
        ]
      }
    ];

    if (sessionStorage.getItem('username') !== null) {
      this.loggedIn = true;
    }
  }

  login(): void {
    if (this.username != "") {
      sessionStorage.setItem('username', this.username);
    }
    this.ngOnInit();
  }

  logout(): void {
    sessionStorage.removeItem('username');
    this.loggedIn = false;
  }

}


