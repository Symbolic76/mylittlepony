import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'locationName'
})
export class LocationNamePipe implements PipeTransform {

  transform(value: string): string {
    let transformedValue = "";
    for (const v of value.toUpperCase()) {
      transformedValue += v + '.'
    }
    return transformedValue;
  }

}
