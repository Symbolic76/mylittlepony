import { LocationNamePipe } from './location-name.pipe';

describe('LocationNamePipe', () => {
  it('create an instance', () => {
    const pipe = new LocationNamePipe();
    expect(pipe).toBeTruthy();
  });

  it('Test the locationPipe', () => {
    const pipe = new LocationNamePipe();
    expect(pipe.transform('roubaix')).toBe('R.O.U.B.A.I.X.');
  })
});