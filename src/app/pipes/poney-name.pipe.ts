import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'poneyName'
})
export class PoneyNamePipe implements PipeTransform {

  transform(name: String, ...args: unknown[]): String {
    name = name.charAt(0).toUpperCase() + name.slice(1);
    return name;
  }

}
