import { Component, OnInit } from '@angular/core';
import { Race } from '../race';
import { PONIES } from '../mock/mock-ponies';
import { PoniesComponent } from '../ponies/ponies.component';
import { PonyService } from '../pony.service';
import { RaceService } from '../race.service';
import { Pony } from '../pony';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-add-race',
  templateUrl: './add-race.component.html',
  styleUrls: ['./add-race.component.css']
})
export class AddRaceComponent implements OnInit {

  race: Race;
  ponies = PONIES;
  poniesSelect: Array<Pony>;
  listdrop: Array<Pony>;
  updateRace: boolean = false;

  constructor(private service: RaceService, private service2: PonyService, private router: Router, private route: ActivatedRoute) { 
    this.race = new Race(0,new Date(),"")
    this.listdrop = [];
    this.poniesSelect = [];
  }

  ngOnInit(): void {
    this.race = new Race(0,new Date(),"")
    this.service2.getAllPonies().subscribe(
      p => {
        console.log("Sub ponies");
        this.poniesSelect = p;
      }
    )
    this.route.params.subscribe(p => {
      if (p.id != undefined) {
        this.updateRace = true;
        this.service.getRace(p.id).subscribe(race => {
          this.race = race;
          this.listdrop = race.ponies;
        });
      } else {
        this.race = new Race(0, new Date(), "")
      }
    });
  }

  onSubmit(): void{
    this.race.ponies = this.listdrop;
    this.updateRace ? this.service.updateRace(this.race) : this.service.addRace(this.race);
    this.router.navigate(["/races"]);
  }
}
