import { Component, Input, OnInit } from '@angular/core';
import { Pony } from '../pony';

@Component({
  selector: 'app-pony-details',
  templateUrl: './pony-details.component.html',
  styleUrls: []
})
export class PonyDetailsComponent implements OnInit {

@Input() pony: Pony = new Pony();

  constructor() { }

  ngOnInit(): void {
  }

}
