import { Component, OnInit } from '@angular/core';
import { RACES } from '../mock/mock-races';
import { Race } from '../race';
import { RaceService } from '../race.service';

@Component({
  selector: 'app-races',
  templateUrl: './races.component.html',
  styleUrls: ['./races.component.css']
})
export class RacesComponent implements OnInit {

  races : Array<Race> = [];
  constructor(private service: RaceService) {
    //this.races = RACES;
   }

  ngOnInit(): void {
    this.service.getAllRaces().subscribe(
      r => {
        console.log("sub races");
        this.races = r;
    });
  }

}
